{
 "metadata": {
  "name": ""
 },
 "nbformat": 3,
 "nbformat_minor": 0,
 "worksheets": [
  {
   "cells": [
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Captura de caracteres acentuados em REGEX\n",
      "=========================================\n",
      "\n",
      "Suponha que temos que capturar duas varia\u00e7\u00f5es da mesma string, uma com acento, e outra sem."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "import re\n",
      "s1 = 'VENDA LIQUIDA'\n",
      "s2 = 'VENDA L\u00cdQUIDA'"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [],
     "prompt_number": 1
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "Um caractere acentuado tem no m\u00ednimo dois bytes, um para o caractere base, e outro para o acento."
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print re.findall(r'LIQUIDA', s1)\n",
      "print re.findall(r'L\u00cdQUIDA', s2)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "['LIQUIDA']\n",
        "['L\\xc3\\x8dQUIDA']\n"
       ]
      }
     ],
     "prompt_number": 2
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "So far, so good. Agora vamos juntar os as duas possibilidades em um regex, para isso utilizaremos a captura de grupo: ``[]``"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print re.findall(r'L[I\u00cd]QUIDA', s1)\n",
      "print re.findall(r'L[I\u00cd]QUIDA', s2)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "['LIQUIDA']\n",
        "[]\n"
       ]
      }
     ],
     "prompt_number": 3
    },
    {
     "cell_type": "markdown",
     "metadata": {},
     "source": [
      "O regex funcionou bem para o caso sem acento. E com acento, o que houve? Acontece que a defini\u00e7\u00e3o de grupo de caracteres ``[]`` espera apenas uma ocorr\u00eancia do caractere informado, e o retorno de ``\u00cd`` como um grupo, s\u00e3o 2 caracteres, lembra?\n",
      "\n",
      "Ent\u00e3o precisaremos permitir que o retorno do grupo possa ser mais que um caracter, isso pode ser feito com os modificadores ``+``, ``*`` ou ``{}``. Vamos espec\u00edficar a quantidade vari\u00e1vel de 1 \u00e0 2:"
     ]
    },
    {
     "cell_type": "code",
     "collapsed": false,
     "input": [
      "print re.findall(r'L[I\u00cd]{1,2}QUIDA', s1)\n",
      "print re.findall(r'L[I\u00cd]{1,2}QUIDA', s2)"
     ],
     "language": "python",
     "metadata": {},
     "outputs": [
      {
       "output_type": "stream",
       "stream": "stdout",
       "text": [
        "['LIQUIDA']\n",
        "['L\\xc3\\x8dQUIDA']\n"
       ]
      }
     ],
     "prompt_number": 4
    }
   ],
   "metadata": {}
  }
 ]
}

=======
iPython
=======

iPython é um prompt interativo com super-poderes. Ele expande as posibilidades
de experimentação/exploração da linguagem de diversas formas.

Instalação
================

Instale via pip:

.. code-block:: bash

    $ pip install ipython


Uso básico
================

Para iniciar o prompt, basta digitar ``ipython`` no console:

.. code-block:: bash

    $ ipython
    IPython 1.1.0 -- An enhanced Interactive Python.
    ?         -> Introduction and overview of IPython's features.
    %quickref -> Quick reference.
    help      -> Python's own help system.
    object?   -> Details about 'object', use 'object??' for extra details.
    In [1]:


ipython notebook
================

O ipython notebook é um prompt Python no browser, só que muito mais do que
isso. Utilizado e idealizado pela comunidade científica, ele permite misturar
as facilidades de um interpretador interativo com documentação. Também permite
processamento distribuído em clusters, visualização integrada de diversos tipos
de media, e muito mais.

Instalação de dependências
--------------------------

Instale as dependências:

.. code-block:: bash

    $ easy_install pyzmq
    $ pip install jinja2
    $ pip install tornado


nbviewer
---------

O `nbviewer <http://nbviewer.ipython.org/>`_ é um site que permite a
visualização de ``ipython notebooks`` online. Basta apontar uma url do arquivo
.ipynb

Vejamos um exemplo: `Como capturar caracteres acentuados com regex
<http://nbviewer.ipython.org/url/colibri.sytes.net/tutorial_python/_static/acentos_em_regex.js>`_.


Recentemente, o Ricardo Duarte portou o livro `Python para desenvolvedores
<http://ark4n.wordpress.com/python/>`_ para a web, utilizando o ipython
notebook. Você pode baixar os exemplos e rodar localmente. Veja o `livro online
<http://ricardoduarte.github.io/python-para-desenvolvedores/>`_.

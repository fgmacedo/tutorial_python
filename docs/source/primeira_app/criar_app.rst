
Criar uma APP
=============

Execute o comando startapp, no :term:`project_root`::

    (env)$ python manage.py startapp materiais


Este comando irá criar uma estrutura básica para a app::

    \---materiais
            models.py
            tests.py
            views.py
            __init__.py


:models.py:  Arquivo onde vão os molelos que mapeiam objetos do banco de dados.
:tests.py:  Testes da aplicação.
:views.py:  Views da aplicação.

Edite o arquivo :file:`coolcad/settings.py`, e adicione a app ``materiais`` na
lista de apps instaladas.::

    INSTALLED_APPS = (
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.sites',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        # Uncomment the next line to enable the admin:
        # 'django.contrib.admin',
        # Uncomment the next line to enable admin documentation:
        # 'django.contrib.admindocs',
        'materiais',
    )

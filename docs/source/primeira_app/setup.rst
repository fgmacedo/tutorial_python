
Setup Django
============

#. Na raíz do repositório, ou :term:`repository_root`, crie um ambiente virtual

    .. code-block:: bash

        $ virtualenv env

#. Ative o ambiente

    No Windows, digite:

    .. code-block:: bash

        $ env\Scripts\activate.bat

    Em GNU Linux/OSX:

    .. code-block:: bash

        $ source env/bin/activate

#. Instale o Django

    .. code-block:: bash

        (env)$ pip install django


#. Crie um arquivo de dependências

    .. code-block:: bash

        (env)$ mkdir requirements
        (env)$ pip freeze >requirements/base.txt

#. Crie um projeto Django

    .. code-block:: bash

        (env)$ mkdir project
        (env)$ python env\Scripts\django-admin.py startproject coolcad project


    O comando ``startproject`` criou a estrutura básica de um projeto Django no diretório ``project``.
    A partir de agora chamaremos o diretório ``project`` de :term:`project_root`.

        .. code-block:: bash

             \---project
                 |   manage.py
                 |
                 \---coolcad
                         settings.py
                         urls.py
                         wsgi.py
                         __init__.py


#. Veja se está funcionando

    #. Com um prompt na raíz do projeto (/project), inicie o servidor de desenvolvimento

        .. code-block:: bash

            (env)$ cd project
            (env)$ python manage.py runserver
            Validating models...

            0 errors found
            Django version 1.5.1, using settings 'coolcad.settings'
            Development server is running at http://127.0.0.1:8000/
            Quit the server with CTRL-BREAK.

    #. Abra um browser em ``localhost:8000``, e verifique se é exibida a mensagem **It worked!**.


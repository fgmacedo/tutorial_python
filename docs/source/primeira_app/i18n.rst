
Internacionalização
===================


Instalando Gettext no Windows
-----------------------------

O suporte à internacionalização do Django é baseado no **GNU Gettext**, este é um **software livre** amplamente utilizado no Linux, Unix e outros sistemas operacionais.

No Windows, será necessário instalarmos uma versão que foi portada.

Para baixá-lo, acesse o `site do projeto no sourceforge <http://sourceforge.net/projects/gettext>`_, e depois clique em **Download**.

Ou se preferir, baixe a `diretamente última versão <http://sourceforge.net/projects/gettext/files/latest/download>`_.


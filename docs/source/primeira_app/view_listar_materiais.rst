
Listar materiais
================

Nossa configuração de rotas em :file:`coolcad/urls.py` tem uma rota que aponta para a view index, na app materiais::

        url(r'^materiais/', 'materiais.views.index'),


Queremos que esta view retorne ao usuário uma lista com todos os materiais cadastrados no banco de dados.

Para isso, vamos precisar do ORM, e do nosso modelo de materiais.

Também não queremos que nossa view seja responsável pela formatação da lista, por isso vamos delegar a parte visual para um template.

Edite o arquivo :file:`materiais/views.py`, conforme segue::

    #coding: utf-8

    from django.shortcuts import render

    from .models import Material


    def index(request):
        materiais = Material.objects.all()
        context = dict(
            materiais=materiais
        )
        return render(request, 'materiais/index.html', context)

Aponte o browser para ``localhost:8000/materiais``. Você deve ver uma mensagem de erro::

    TemplateNotExist:  ...

O Django está nos dizendo que não encontrou o template ``materiais/index.html``. Que nós ainda não criamos. Criaremos agora.

Localize o diretório ``templates`` no :term:`project_root`. Nele, crie um
diretório ``materiais``, e em ``materiais`` um arquivo ``index.html``.

O caminho completo será ``project/templates/materiais/index.html``.

Edite este arquivo, conforme segue:

.. code-block:: django

    <h1>Olá, minha primeira view em template.</h1>
    <ul>
    {% for m in materiais %}
        <li>{{m.descricao}}</li>
    {%endfor%}
    </ul>

Aponte o browser para ``localhost:8000/materiais``. Você deve ver a listagem dos materiais cadastrados no banco.

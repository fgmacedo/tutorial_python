
************************************
Class-based views e Views genéricas
************************************

A partir do Django 1.3, surgiram as class-based views, que são uma forma mais
orientada a objetos de construir suas views.

O processo interno continua o mesmo, as rotas continuam recebendo uma função
com a assinatura de uma view::

    def view(request, *args, **kwargs):
        pass

Mas o método da view será gerado a partir de uma classe, que qualquer um
poderia ter construído. Ou seja, as class-based views são opcionais.

No entanto, o conceito é poderoso e pode facilitar o reúso de código em
diversas formas, de modo que algumas views de uso genérico já são distribuídas
com o framework.


View
====

Classe base para as class-based views. Define métodos correspondentes para os
métodos HTTP.


TemplateView
============

Atalho para renderizar um template diretamente.

Vamos utilizar para fazer uma página Home.

View home, em :file:`coolcad/views.py`:

.. code-block:: python

    #coding: utf-8

    from django.views.generic import TemplateView

    class HomeView(TemplateView):
        template_name = 'coolcad/home.html'

Adicionando a view nas rotas, em :file:`coolcad/urls.py`:

.. code-block:: python
    :emphasize-lines: 6, 10

    from django.conf.urls import patterns, include, url

    from django.contrib import admin
    admin.autodiscover()

    from . import views

    urlpatterns = patterns(
        '',
        url(r'^$', views.HomeView.as_view()),
        url(r'^materiais/', include('materiais.urls', namespace="materiais")),
        url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
        url(r'^admin/', include(admin.site.urls)),
    )

E por fim, crie o template :file:`templates/coolcad/home.html`:

.. code-block:: django

    {% extends "base.html" %}

    {% block content %}
    <div class="jumbotron">
            <h1>Coolcad</h1>
            <p class="lead">Bem vindo ao cadastro.</p>
            <p><a class="btn btn-lg btn-success" href="{% url "materiais:index"%}">Cadastre alguma coisa</a></p>
    </div>
    {% endblock content %}


ListView
========

View genérica para listar objetos de um modelo. Tem suporte a paginação.

Vamos refatorar a listagem de materiais.

Edite o arquivo :file:`materiais/views.py`, para importar a view ``ListView``::

    from django.views.generic import ListView

Refatore a view ``index`` para ser uma classe que herde de ListView. Localize o
código::

    def index(request):
        materiais = Material.objects.all()
        context = dict(
            nome="Colibri",
            materiais=materiais
        )
        return render(request, 'materiais/index.html', context)

E altere para::

    class IndexView(ListView):
        model = Material
        context_object_name = 'materiais'
        template_name = 'materiais/index.html'

Vamos alterar a rota para usar a CBV que criamos. Localize o código::

    url(r'^$', views.index, name='index'),

E altere para::

    url(r'^$', views.IndexView.as_view(), name='index'),

Com estes passos, você já pode ver a lista no site.

Utilizando as convenções de nomes
---------------------------------

Utilizamos as propriedades ``context_object_name`` e ``template_name`` para
manter compatibilidade com o nome do template e variável que utilizamos na
primeira versão da view ``index``.

template_name

    Por padrão, a view genérica **ListView** irá procurar por um template no
    formato ``<application>/<model>_list.html``.

context_object_name

    Por padrão, a view genérica **ListView** irá disponibilizar para o contexto
    do template as variáveis ``<model>_list`` e ``object_list``.

Vamos alterar a view **IndexView** para utilizar os nomes no formato padrão,
deste modo não será necessário informar o nome da lista no contexto, nem o nome
do template.

Edite o arquivo :file:`materiais/views.py`, localize o código::

    class IndexView(ListView):
        model = Material
        context_object_name = 'materiais'
        template_name = 'materiais/index.html'

E altere para::

    class IndexView(ListView):
        model = Material



Altere o nome do arquivo de template da lista de
:file:`templates/materiais/index.html` para
:file:`templates/materiais/material_list.html`

Edite o template :file:`templates/materiais/material_list.html` para utilizar a
variável ``material_list`` ao invés de ``materiais``:

.. code-block:: django
    :emphasize-lines: 4, 8

    {% extends "materiais/materiais_base.html" %}

    {%block content %}
        {% if not material_list %}
            <p>Sem materiais para exibir.</p>
        {%else%}
            <ul>
            {% for m in material_list %}
                <li><a href="{% url "materiais:detalhe" m.id %}">{{m.descricao}}</a></li>
            {%endfor%}
            </ul>
        {%endif%}
    {%endblock%}

Paginação
---------

Quando existem muitos registros para exibir, pode ser necessário dividir os
registros em páginas, exibindo uma quantidade limitada de registros por página.
Isso desonera o banco de dados e facilita a visualização para o usuário final.

A **ListView** já suporta paginação nativamente, e um dos modos de ativar a
paginação é informar um valor diferente de zero para a propriedade
``paginate_by``.

Vamos alterar a view **IndexView** para paginar a cada 10 registros.

Edite o arquivo :file:`materiais/views.py`, localize o código::

    class IndexView(ListView):
        model = Material

E altere para::

    class IndexView(ListView):
        model = Material
        paginate_by = 10

Com esta alteração, a view incluirá automaticamente algumas variáveis ao
contexto:

    paginator

        Dados sobre a paginação (quantidade de páginas, quantidade total de
        registros, etc.)

    page_obj

        Dados sobre a página atual (número da página, tem página anterior?, tem
        próxima página?, etc.)

    is_paginated

        Propriedade booleana que indica se há paginação, ou seja, se a
        quantidade de registros da lista é maior que a quantidade de registros
        por página.


Por ser uma característica de qualquer **ListView**, podemos criar um template
genérico de paginação.

Crie o arquivo :file:`templates/_pagination.html`:

.. code-block:: django

    {% if is_paginated %}
    <ul class="pagination">
        {% if page_obj.has_previous %}
            <li><a href="?page={{ page_obj.previous_page_number }}">&laquo;</a></li>
        {%else%}
            <li class="disabled"><a href="#">&laquo;</a></li>
        {% endif %}
        {% for page in paginator.page_range %}
            <li {% if page_obj.number == page %}class="active"{% endif %}>
              <a href="?page={{ page }}">{{ page }}</a>
            </li>
        {% endfor %}
        </span>
        {% if page_obj.has_next %}
            <li><a href="?page={{ page_obj.next_page_number }}">&raquo;</a></li>
        {%else%}
            <li class="disabled"><a href="#">&raquo;</a></li>
        {% endif %}
    </ul>
    {% endif %}


Para exibir a paginação, edite o template
:file:`templates/materiais/material_list.html` para incluir o arquivo
:file:`templates/_pagination.html`:

.. code-block:: django
    :emphasize-lines: 7, 13

    {% extends "materiais/materiais_base.html" %}

    {%block content %}
        {% if not material_list %}
            <p>Sem materiais para exibir.</p>
        {%else%}
            {% include "_pagination.html" %}
            <ul>
            {% for m in material_list %}
                <li><a href="{% url "materiais:detalhe" m.id %}">{{m.descricao}}</a></li>
            {%endfor%}
            </ul>
            {% include "_pagination.html" %}
        {%endif%}
    {%endblock%}


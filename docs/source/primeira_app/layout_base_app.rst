
Layout base para a APP
======================

Problema

    Já temos um layout padrão para o projeto, mas queremos definir algumas
    características da página que serão comuns para a app de materiais.

Solução

    Vamos personalizar o layout base para a APP de materiais.


Crie um arquivo :file:`templates/materiais/materiais_base.html`, conforme
segue:

.. code-block:: django

    {% extends "base.html" %}

    {%block main_content %}
    <h1>Materiais</h1>
    <div class="row">
        <div class="col-lg-2">
            <ul class="nav nav-stacked">
                <li><a href="{% url "materiais:index" %}">Lista</a></li>
                <li><a href="#">Adicionar</a></li>
            </ul>
        </div>
        <div class="col-lg-10">{%block content%}{%endblock%}</div>
    </div>
    {%endblock%}


Edite o template pai nos templates existentes:

- templates/materiais/detalhe.html
- templates/materiais/index.html

    Procure pelo código

    .. code-block:: django

        {% extends "base.html" %}

    e altere para:

    .. code-block:: django

        {% extends "materiais/materiais_base.html" %}

Seu app deve estar ± assim:

.. figure:: img/layout_base_app.png

    App com layout personalizado.

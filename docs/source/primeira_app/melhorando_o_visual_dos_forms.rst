
*****************************
Melhorando o visual dos forms
*****************************

Vamos utilizar o pacote `django-crisp-forms <http://django-crispy-
forms.readthedocs.org/>`_.

Este app fornece alguns filtros que permitem renderizar os controles dos forms
Django utilizando toolkits de UI, como Foundation, Bootstrap e outros.

Tarefas:

    - Atualize o Twitter Bootstrap (3.0.0);
    - `Instale o pacote django-crisp-forms <http://django-crispy-forms.readthedocs.org/en/latest/install.html#installing-django-crispy-forms>`_;
    - Atualize o arquivo :file:`requirements/base.txt`.
    - Informe a configuração do template pack utilizado::

        CRISPY_TEMPLATE_PACK = 'bootstrap3'


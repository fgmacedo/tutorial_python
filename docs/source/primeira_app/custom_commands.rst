=========================
Comandos de administração
=========================

Importar grupo
==============

Estrutura de diretórios
-----------------------

Os comandos ficam em um diretório especial da aplicação::

    materiais/
    │   __init__.py
    │   ...
    │   models.py
    │
    └───management
        │   __init__.py
        │
        └───commands
                __init__.py
                importar_grupo.py


Crie os arquivos ``__init__.py`` (vazios). Eles são necessários para
identificar os diretórios como pacotes Python.

Crie o arquivo ``importar_grupo.py``::

    # coding: utf-8
    from django.core.management.base import BaseCommand, CommandError

    from materiais.models import GrupoDeMaterial

    class Command(BaseCommand):
        args = 'caminho'
        help = 'Importa grupos em um json no banco de dados'

        def handle(self, *args, **options):
            caminho = args[0]
            import json
            with open(caminho) as f:
                dados = json.loads(f.read())

                # sua lógica aqui

Agora você já pode ver seu comando registrado no help do admin:

.. code-block:: bash

    (env) python manage.py help
    ...

    [materiais]
        importar_grupo

E também o help do seu comando customizado:

.. code-block:: bash

    (env) python manage.py help importar_grupo
    Usage: manage.py importar_grupo [options] caminho

    Importa um json no banco


E por fim, pode executar seu comando:

.. code-block:: bash

    (env) python manage.py importar_grupo <caminho do arquivo de dados>


.. seealso:: `Escrevendo comandos customizados de administração
    <https://docs.djangoproject.com/en/dev/howto/custom-management-commands/>`_



Dica: Módulo json
-----------------

De string para objetos::

    import json
    json_string = "[{"a": 1}, {"b": 2}]"
    dados = json.loads(json_string)

De dados para string::

    import json
    dados = [{"a": 1}, {"b": 2}]
    json_string = json.dumps(dados)


Resolução
=========

Edite o arquivo ``importar_grupo.py`` para que fique assim::

    # coding: utf-8

    import os
    import json
    from django.core.management.base import BaseCommand, CommandError

    from materiais.models import GrupoDeMaterial

    class Command(BaseCommand):
        args = 'caminho'
        help = 'Importa grupos em um json no banco de dados'

        def handle(self, *args, **options):
            if not args:
                raise CommandError('Nenhum arquivo informado.')
            caminho = args[0]

            if not os.path.exists(caminho):
                msg = 'Arquivo informado ({}) não existe.'.format(caminho)
                raise CommandError(msg)

            with open(caminho) as f:
                dados = json.loads(f.read())

            for item in dados:
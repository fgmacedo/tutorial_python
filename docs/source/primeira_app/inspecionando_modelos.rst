Inspecionando os modelos
------------------------

Com nossa representação dos modelos para cadastro de materiais criada, 
iremos sincronizar o modelo com o banco de dados.

O Django fornece o comando ``syncdb`` para aplicar o modelo ao banco de dados.
No terminal, execute:

.. code-block:: bash

    (env)$ python manage.py syncdb
    Creating tables ...
    Creating table materiais_grupodematerial
    Creating table materiais_material
    Installing custom SQL ...
    Installing indexes ...
    Installed 0 object(s) from 0 fixture(s)


Como o banco sincronizado, podemos através do ORM inspecionar os modelos que
criamos em um terminal iterativo. No diretório :term:`project_root`, digite:

.. code-block:: bash

    (env)$ python manage.py shell

No prompt interativo, importe o módulo dos modelos e insira alguns registros.

Vamos inserir um grupo de material::

    >>> from materiais.models import GrupoDeMaterial
    >>> g2 = GrupoDeMaterial(nome="Regrigerante", tipo=2)
    >>> g2.save()

Isto resultará em uma instrução SQL ``INSERT``. Note que o Django não fará
nenhum acesso ao banco até que você chame o método :py:func:`save`.

.. seealso::

    :py:func:`save` pode receber opções avançadas não descritas aqui. Veja a
    documentação de `save <https://docs.djangoproject.com/en/1.5/ref/models/
    instances/#django.db.models.Model.save>`_ para detalhes completos.

    Para criar e salvar um objeto em um único passo, use o método `create
    <https://docs.djangoproject.com/en/1.5/ref/models/querysets/#django.db.models.
    query.QuerySet.create>`_.


Tente criar mais alguns registros de grupos e materiais.

.. note::  Consulte a documentação do Django para descobrir como fazer
    `queries <https://docs.djangoproject.com/en/1.5/topics/db/queries/>`_

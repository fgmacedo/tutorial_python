
Primeira APP
============

Vamos fazer uma APP simples para cadastro de materiais e grupos.

Primeiro, precisamos de um ambiente virtual, nele instalaremos as dependências.
Em seguida iniciaremos um projeto Django, com as configurações mínimas, e por
fim, nossa primeira app.


.. toctree::
   :maxdepth: 2

   fundamentos
   setup
   configuracoes
   criar_app
   primeiros_models
   inspecionando_modelos
   admin
   descricao_modelos
   primeira_view
   view_listar_materiais
   heranca_de_templates
   arquivos_estaticos
   organizando_urlconfs
   layout_base_app
   forms
   custom_commands
   melhorando_o_visual_dos_forms
   views_genericas
   django_debug_toolbar
   i18n


Django debug toolbar
====================

O projeto `django-debug-toolbar <https://github.com/django-debug-toolbar
/django-debug-toolbar>`_ facilita a inspeção das páginas, exibindo uma barra
com diversas informações em painéis configuráveis:

* Versão do Django
* Tempo da requisição
* As configurações atuais
* Cabeçalhos HTTP
* Variáveis recebidas via GET/POST
* Variáveis armazenadas cookie e na sessão
* Templates e contextos utilizados, e os caminhos dos templates
* Consultas SQL realizadas incluindo o tempo, e link para o comando "Explain"
     de cada query
* Lista dos sinais enviados, seus argumentos e receptores
* Logs emitidos via sistema built-in de logos do Python


Instalação
----------

#. Com o ambiente virtual ativado, instale via PIP:

    .. code-block:: bash

        (env)$ pip install django-debug-toolbar

#. Crie um arquivo específico para requisitos de desenvolvimento, que herde dos
    requisitos básicos do projeto.

    Crie um arquivo :file:`requirements/dev.txt`:

    .. code-block:: text

        -r base.txt
        django-debug-toolbar==0.9.4


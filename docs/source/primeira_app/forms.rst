
Formulários
===========

Plain forms
-----------

Objetivo

    - Aprender os elementos HTML básicos de um form;
    - Descobrir como receber parâmetros POST em uma view do Django;

Solução

    Faremos uma view simples, que utiliza os parâmetros recebidos via POST.

    O formulário será feito em HTML puro.


Crie um link para a página Adicionar material, altere o arquivo
:file:`templates/materiais/materiais_base.html`, conforme segue:

.. code-block:: django
    :emphasize-lines: 9


    {% extends "base.html" %}

    {%block main_content %}
    <h1>Materiais</h1>
    <div class="row">
        <div class="col-lg-2">
            <ul class="nav nav-stacked">
                <li><a href="{% url "materiais:index" %}">Lista</a></li>
                <li><a href="{% url "materiais:adicionar_plain" %}">Adicionar</a></li>
            </ul>
        </div>
        <div class="col-lg-10">{%block content%}{%endblock%}</div>
    </div>
    {%endblock%}


A rota do link ``materiais:adicionar_plain`` ainda não existe, adicione em
:file:`materiais/urls.py`, apontando para uma view de mesmo nome:


.. code-block:: python
    :emphasize-lines: 10

    #coding: utf-8

    from django.conf.urls import patterns, url

    from . import views

    urlpatterns = patterns(
        '',
        url(r'^$', views.index, name='index'),
        url(r'^adicionar_plain/$', views.adicionar_plain, name='adicionar_plain'),
        url(r'^(?P<material_id>\d+)', views.detalhe, name="detalhe"),
    )


Crie uma view ``adicionar_plain``, em :file:`materiais/views.py`, e importe o
modelo ``GrupoDeMaterial``.

    Localize a linha::

        from .models import Material

    E altere para::

        from .models import Material, GrupoDeMaterial

    Inclua a view:

    .. code-block:: python

        def adicionar_plain(request):
            grupos = GrupoDeMaterial.objects.all()
            context = dict(
                grupos=grupos
            )
            return render(request, 'materiais/adicionar_plain.html', context)


Crie um template :file:`templates/materiais/adicionar_plain.html`:


.. code-block:: django

    {% extends "materiais/materiais_base.html" %}

    {%block content %}
    <div class="row">
        <h3>Cadastrar material</h3>
        <form action="{% url "materiais:adicionar_plain"%}" method="POST">
            {% csrf_token %}
            <input type="text" class="form-control" name="codigo" placeholder="Código" /><br />
            <input type="text" class="form-control" name="descricao" placeholder="Descrição" /><br />
            <input type="text" class="form-control" name="unidade" placeholder="Unidade" /><br />
            <select class="form-control" name="grupo">
            {% for grupo in grupos %}
                <option value="{{grupo.id}}">{{grupo}}</option>
            {% endfor %}
            </select><br />
            <button type="submit" class="btn btn-default">Enviar</button>
        </form>
    </div>
    {%endblock%}

.. seealso:: `csrf_token <https://docs.djangoproject.com/en/dev/ref/contrib/csrf/>`_

    O Django possui um sistema de proteção contra `Cross-site request forgery
    <http://www.squarefree.com/securitytips/web-developers.html#CSRF>`_. Onde
    um site pode tentar enviar requisições que alteram estado no servidor
    (POST, PUT, DELETE...) em nome do usuário autenticado no seu sistema a
    partir de outro domínio (cross-site). Geralmente o ataque envolve
    Javascript para submeter dados de um form automaticamente.


Com estes passos, você já pode ver o formulário na página, porém, nada de útil
acontece no servidor ao enviarmos o formulário.

Agora edite a view ``adicionar_plain`` para criar um material com os dados do
formulário:

.. code-block:: python
    :linenos:
    :emphasize-lines: 6-13

    def adicionar_plain(request):
        grupos = GrupoDeMaterial.objects.all()
        context = dict(
            grupos=grupos
        )
        if request.POST:
            material = Material()
            material.codigo = request.POST['codigo']
            material.descricao = request.POST['descricao']
            material.unidade = request.POST['unidade']
            material.grupo = GrupoDeMaterial.objects.get(pk=request.POST['grupo'])
            material.save()
            context['material'] = material

        return render(request, 'materiais/adicionar_plain.html', context)


Note que na linha 13 adicionamos o material criado ao contexto do template,
vamos utilizar esta informação para exibir uma mensagem de sucesso.

Edite o template :file:`templates/materiais/adicionar_plain.html`:

.. code-block:: django
    :emphasize-lines: 6-11

    {% extends "materiais/materiais_base.html" %}

    {%block content %}
    <div class="row">
        <h3>Cadastrar material</h3>
        {% if material %}
        <div class="alert alert-success">
            Material {{material}} criado com sucesso.
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        {% endif %}

        <form action="{% url "materiais:adicionar_plain"%}" method="POST">
            {% csrf_token %}
            <input type="text" class="form-control" name="codigo" placeholder="Código" /><br />
            <input type="text" class="form-control" name="descricao" placeholder="Descrição" /><br />
            <input type="text" class="form-control" name="unidade" placeholder="Unidade" /><br />
            <select class="form-control" name="grupo">
            {% for grupo in grupos %}
                <option value="{{grupo.id}}">{{grupo}}</option>
            {% endfor %}
            </select><br />
            <button type="submit" class="btn btn-default">Enviar</button>
        </form>
    </div>
    {%endblock%}


.. note:: Bônus

    :EASY: Altere a mensagem de sucesso incluindo um link para o material recém criado.
    :HARD: Altere a view para só aceitar usuários autenticados. Dica: use o decorador `login_required <https://docs.djangoproject.com/en/1.5/topics/auth/default/#the-login-required-decorator>`_.


Class-based forms
-----------------

Objetivo

    Criação/validação de formulários server-side simplificada.

    Aprender o uso básico dos forms do Django, que podem ser utilizados para
    centralizar a validação de dicionários (request.POST é um dicionário)

Solução

    Faremos um formulário para inclusão de materiais, nos mesmos moldes do
    formulário que criamos manualmente, mas agora utilizado os forms do Django.



Crie um link para a página ``Adicionar material class form``, altere o arquivo
:file:`templates/materiais/materiais_base.html`, conforme segue:

.. code-block:: django
    :emphasize-lines: 9-10


    {% extends "base.html" %}

    {%block main_content %}
    <h1>Materiais</h1>
    <div class="row">
        <div class="col-lg-2">
            <ul class="nav nav-stacked">
                <li><a href="{% url "materiais:index" %}">Lista</a></li>
                <li><a href="{% url "materiais:adicionar_plain" %}">Adicionar plain</a></li>
                <li><a href="{% url "materiais:adicionar_class" %}">Adicionar class</a></li>
            </ul>
        </div>
        <div class="col-lg-10">{%block content%}{%endblock%}</div>
    </div>
    {%endblock%}


A rota do link ``materiais:adicionar_class`` ainda não existe, adicione em
:file:`materiais/urls.py`, apontando para uma view de mesmo nome:

.. code-block:: python
    :emphasize-lines: 10

    #coding: utf-8

    from django.conf.urls import patterns, url

    from . import views

    urlpatterns = patterns(
        '',
        url(r'^$', views.index, name='index'),
        url(r'^adicionar_plain/$', views.adicionar_plain, name='adicionar_plain'),
        url(r'^adicionar_class/$', views.adicionar_class, name='adicionar_class'),
        url(r'^(?P<material_id>\d+)', views.detalhe, name="detalhe"),
    )


Crie uma view ``adicionar_class``, em :file:`materiais/views.py`:

.. code-block:: python

    def adicionar_class(request):
        context = dict()
        return render(request, 'materiais/adicionar_class.html', context)


Crie um template :file:`templates/materiais/adicionar_class.html`:

.. code-block:: django

    {% extends "materiais/materiais_base.html" %}

    {% block content %}
    <div class="row">
        {%if material%}
        <div class="alert alert-success">
            Material {{material}} adicionado com sucesso.
        </div>
        {%endif%}
        <h3>Cadastrar material</h3>
        <form action="{% url "materiais:adicionar_class" %}" method="POST">
            {% csrf_token %}
            {{form.as_p}}
            <button type="submit" class="btn btn-default">Enviar</button>
        </form>
    </div>
    {% endblock content %}


Agora utilizaremos um Form do Django, crie um arquivo
:file:`<project_root>/materiais/forms.py`:

.. code-block:: python

    #coding: utf-8

    from django import forms

    from .models import GrupoDeMaterial

    class MaterialForm(forms.Form):
        codigo = forms.CharField(label=u'código', max_length=20)
        descricao = forms.CharField(label=u'descrição', max_length=100)
        unidade = forms.CharField(label=u'unidade', max_length=2)
        grupo = forms.ModelChoiceField(
            label=u'grupo',
            queryset=GrupoDeMaterial.objects
        )


Na view ``adicionar_class``, iremos  adicionar o form que criamos no contexto.

Procure a linha::

    from .models import Material, GrupoDeMaterial

E altere para::

    from .models import Material, GrupoDeMaterial
    from .forms import MaterialForm

Altere a view para retornar o form:

.. code-block:: python
    :emphasize-lines: 2-5

    def adicionar_class(request):
        context = dict(
            form=MaterialForm()
        )
        return render(request, 'materiais/adicionar_class.html', context)

Com estes passos, já é possível exibir o form em nossa página. Vamos editar a
view para salvar os dados ao modelo:

.. code-block:: python

    def adicionar_class(request):
        context = dict()
        if request.POST:
            form=MaterialForm(request.POST)
            if form.is_valid():
                material = Material()
                material.codigo = form.cleaned_data['codigo']
                material.descricao = form.cleaned_data['descricao']
                material.unidade = form.cleaned_data['unidade']
                material.grupo = form.cleaned_data['grupo']
                material.save()
                context['material'] = material
        else:
            form=MaterialForm()

        context['form'] = form
        return render(request, 'materiais/adicionar_class.html', context)


.. seealso:: Documentação do Django sobre `class-based forms
    <https://docs.djangoproject.com/en/dev/topics/forms/>`_.



Model-forms
-----------

Problema

    Inclusão/atualização de dados no sistema, baseada em modelos já existentes.

    Não queremos repetir a definição dos modelos em formulários, reaproveitando
    e centralizando as validações de inclusão nos modelos.

Solução

    Utilizaremos forms auto-gerados a partir de modelos.


Crie um link para a página ``Adicionar material model form``, altere o arquivo
:file:`templates/materiais/materiais_base.html`, conforme segue:

.. code-block:: django
    :emphasize-lines: 11


    {% extends "base.html" %}

    {%block main_content %}
    <h1>Materiais</h1>
    <div class="row">
        <div class="col-lg-2">
            <ul class="nav nav-stacked">
                <li><a href="{% url "materiais:index" %}">Lista</a></li>
                <li><a href="{% url "materiais:adicionar_plain" %}">Adicionar plain</a></li>
                <li><a href="{% url "materiais:adicionar_class" %}">Adicionar class</a></li>
                <li><a href="{% url "materiais:adicionar_model" %}">Adicionar model</a></li>
            </ul>
        </div>
        <div class="col-lg-10">{%block content%}{%endblock%}</div>
    </div>
    {%endblock%}


A rota do link ``materiais:adicionar_model`` ainda não existe, adicione em
:file:`materiais/urls.py`, apontando para uma view de mesmo nome:

.. code-block:: python
    :emphasize-lines: 11

    #coding: utf-8

    from django.conf.urls import patterns, url

    from . import views

    urlpatterns = patterns(
        '',
        url(r'^$', views.index, name='index'),
        url(r'^adicionar_plain/$', views.adicionar_plain, name='adicionar_plain'),
        url(r'^adicionar_class/$', views.adicionar_class, name='adicionar_class'),
        url(r'^adicionar_model/$', views.adicionar_model, name='adicionar_model'),
        url(r'^(?P<material_id>\d+)', views.detalhe, name="detalhe"),
    )


Crie uma view ``adicionar_model``, em :file:`materiais/views.py`:

.. code-block:: python

    def adicionar_model(request):
        context = dict()
        return render(request, 'materiais/adicionar_class.html', context)


**Edite** o template :file:`templates/materiais/adicionar_class.html`:

.. code-block:: django
    :emphasize-lines: 6

    {% extends "materiais/materiais_base.html" %}

    {% block content %}
    <div class="row">
        {%if material%}
        <div class="alert alert-success">
            Material {{material}} adicionado com sucesso.
        </div>
        {%endif%}
        <h3>Cadastrar material</h3>
        <form action="." method="POST">
            {% csrf_token %}
            {{form.as_p}}
            <button type="submit" class="btn btn-default">Enviar</button>
        </form>
    </div>
    {% endblock content %}


Agora utilizaremos um ModelForm do Django, **edite** o arquivo
:file:`<project_root>/materiais/forms.py`:

Procure a linha::

    from .models import GrupoDeMaterial

E altere para::

    from .models import Material, GrupoDeMaterial

E adicione o novo formulário:

.. code-block:: python

    class MaterialModelForm(forms.ModelForm):
        class Meta:
            model = Material


Na view ``adicionar_model``, iremos  adicionar o form que criamos no contexto.

Procure a linha::

    from .forms import MaterialForm

E altere para::

    from .forms import MaterialForm, MaterialModelForm

Altere a view para retornar o form:

.. code-block:: python
    :emphasize-lines: 2-5

    def adicionar_model(request):
        context = dict(
            form=MaterialModelForm()
        )
        return render(request, 'materiais/adicionar_class.html', context)

Com estes passos, já é possível exibir o form em nossa página. Vamos editar a
view para salvar os dados ao modelo:

.. code-block:: python

    def adicionar_model(request):
        context = dict()
        if request.POST:
            form = MaterialModelForm(request.POST)
            if form.is_valid():
                context['material'] = form.save()
        else:
            form = MaterialModelForm()

        context['form'] = form
        return render(request, 'materiais/adicionar_class.html', context)


Arquivos estáticos
==================

jQuery
------

O jQuery é uma biblioteca javascript que torna uniforme as diversas diferenças
entre inplementações de browsers. Além disso, fornece uma rica API para acesso
aos elementos de sua página.

Em torno do jQuery vive um rico ecosistema, sendo pré-requisito para diversos
outros frameworks, incluindo o Bootstrap.

Vá para http://jquery.com/, clique em download, e baixe o arquivo não
comprimido, para a versão da linha 1.x (1.10.2 quando este tutorial foi
escrito). A linha 2.x não suporta as versões 6, 7 e 8 do Internet Explorer. Se
isso não for um problema para você, o código da versão 2.x é mais limpo.

Copie o arquivo do jQuery no diretório de arquivos estáticos::

    static_files/js/jquery.js



Twitter Bootstrap
-----------------

Vamos melhorar o visual do nosso cadastro incorporando o `Twitter Bootstrap
<http://getbootstrap.com/>`_.

Acesse o site http://getbootstrap.com/, clique em download, e baixe o arquivo
zip em um diretório temporário.

O pacote possui dois arquivos ``css``, e dois arquivos ``js``::

    ├───css
    │       bootstrap.css
    │       bootstrap.min.css
    │
    └───js
            bootstrap.js
            bootstrap.min.js

Copie os dois diretórios (js e css) para o diretório de arquivos estáticos do
projeto ``project/static_site``, configurado em ``coolcad/settings.py``.

Vamos partir de um layout básico, disponível em
http://examples.getbootstrap.com/starter-template/index.html .  Abra a página
em seu navegador, clique com o botão direito, e selecione a opção "Exibir
código fonte da página".

Copie o html da página e sobrescreva o conteúdo do nosso ``base.html``,
conforme segue:

.. code-block:: html

    <!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Starter Template for Bootstrap</title>

        <!-- Bootstrap core CSS -->
        <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="starter-template.css" rel="stylesheet">
      </head>

      <body>

        <div class="navbar navbar-inverse navbar-fixed-top">
          <div class="container">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Project name</a>
            <div class="nav-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#contact">Contact</a></li>
              </ul>
            </div><!--/.nav-collapse -->
          </div>
        </div>

        <div class="container">

          <div class="starter-template">
            <h1>Bootstrap starter template</h1>
            <p class="lead">Use this document as a way to quickly start any new project.<br> All you get is this text and a mostly barebones HTML document.</p>
          </div>

        </div><!-- /.container -->

      </body>
    </html>


Layout básico do projeto
------------------------

Vamos utlizar a template tag ``static`` para obter o caminho dos recursos
estáticos.

Para facilitar a integração com Django APPs de terceiros, utilizaremos
``content`` como nome do bloco principal.

Edite o arquivo para que fique assim:

.. code-block:: django
    :emphasize-lines: 10,13-14,26,29-31,37-47,48-51

    {% load static %}
    <!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>{%block titulo %}Coolcad{%endblock%}</title>

        <!-- Bootstrap core CSS -->
        <link href="{% static "css/bootstrap.css" %}" rel="stylesheet">
        <link href="{% static "css/app.css" %}" rel="stylesheet">
      </head>

      <body>

        <div class="navbar navbar-inverse navbar-fixed-top">
          <div class="container">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Coolcad</a>
            <div class="nav-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="#">Materiais</a></li>
                <li><a href="#configuracoes">Configurações</a></li>
                <li><a href="#relatorios">Relatórios</a></li>
              </ul>
            </div><!--/.nav-collapse -->
          </div>
        </div>

        <div class="container">
        {%block main_content %}
        {%block content %}
          <div>
            <h1>Coolcad - Página principal</h1>
            <p class="lead">Sobrescreva este bloco.</p>
          </div>
        {% endblock content %}
        {% endblock main_content %}
        </div><!-- /.container -->

        {% block "js" %}
        <script type="text/javascript" src="{% static "js/jquery.js" %}"></script>
        <script type="text/javascript" src="{% static "js/bootstrap.js" %}"></script>
        {%endblock%}
      </body>
    </html>


No diretório de arquivos estáticos, crie um arquivo para os estilos da
aplicação, iremos chamá-lo de ``app.css``.

Crie e edite o arquivo :file:`static_site/css/app.css`:

.. code-block:: css

    body {
      padding-top: 50px;
    }

Edite o nome do bloco principal nos templates existentes:

- templates/materiais/detalhe.html
- templates/materiais/index.html

    Procure pelo código

    .. code-block:: django

        {%block conteudo %}

    e altere para:

    .. code-block:: django

        {%block content %}
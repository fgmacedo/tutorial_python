
Fundamentos
===========


Um **projeto Django** é uma aplicação web criada e mantida através do
framework web Django.

**Django apps** são pequenas bibliotecas projetadas para representar um único
aspecto do projeto.     Um projeto Django é feito de vários Django apps.
Alguns destes apps são internos ao projeto e      nunca serão reutilizados;
outros são bibliotecas de terceiros.

**Bibliotecas de terceiros** são simples apps plugáveis, reutilizáveis, que
foram empacotadas com as ferramentas de empacotamento do Python.

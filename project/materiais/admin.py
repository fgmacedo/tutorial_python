#coding: utf-8

from django.contrib import admin

from .models import GrupoDeMaterial, Material


admin.site.register(GrupoDeMaterial)
admin.site.register(Material)
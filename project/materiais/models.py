# coding: utf-8

from django.db import models

GRUPO_ALIMENTOS, GRUPO_ALCOOLICOS, GRUPO_NAO_ALCOOLICOS, GRUPO_OUTROS = 1, 2, 3, 4

TIPO_GRUPO_CHOICES = (
    (GRUPO_ALIMENTOS, u'Alimentos'),
    (GRUPO_ALCOOLICOS, u'Alcoólicos'),
    (GRUPO_NAO_ALCOOLICOS, u'Não alcoólicos'),
    (GRUPO_OUTROS, u'Outros'),
)
TIPO_GRUPO_CHOICES_DICT = dict(TIPO_GRUPO_CHOICES)

class GrupoDeMaterial(models.Model):
    nome = models.CharField(max_length=50)
    tipo = models.IntegerField(choices=TIPO_GRUPO_CHOICES, default=GRUPO_OUTROS)

    def __unicode__(self):
        return u"{} - {}".format(self.nome, TIPO_GRUPO_CHOICES_DICT[self.tipo])

class Material(models.Model):
    codigo = models.CharField(u'código', max_length=20)
    descricao = models.CharField(u'descrição', max_length=100)
    unidade = models.CharField(u'unidade', max_length=2)
    grupo = models.ForeignKey(GrupoDeMaterial)

    def __unicode__(self):
        return u"{} - {}".format(self.codigo, self.descricao)


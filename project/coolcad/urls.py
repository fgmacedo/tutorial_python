from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from . import views

urlpatterns = patterns(
    '',
    url(r'^$', views.HomeView.as_view()),
    url(r'^materiais/', include('materiais.urls', namespace="materiais")),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

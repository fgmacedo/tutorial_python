
Tutorial de Python com Django
=============================

Instalação
----------

Você deve ter ao menos Python e pip instalados.

Instale o virtualenv, se não tiver:

.. code-block:: bash

    $ pip install virtualenv


Faça o clone do projeto. Crie e ative um ambiente virtual:


.. code-block:: bash

    $ git clone https://fgmacedo@bitbucket.org/fgmacedo/tutorial_python.git
    $ cd tutorial_python
    $ virtualenv env
    $ env\Scripts\activate.bat


Instale as dependências:

.. code-block:: bash

    (env)$ pip install -r requirements/base.txt